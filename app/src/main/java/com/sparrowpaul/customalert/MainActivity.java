package com.sparrowpaul.customalert;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.sparrowpaul.customalert.fragments.VideoPlayerDialogFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        //     playVideo();
        //custom alert dialog
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                VideoPlayerDialogFragment videoPlayerDialogFragment = new VideoPlayerDialogFragment();
                videoPlayerDialogFragment.setRetainInstance(true);
                videoPlayerDialogFragment.show(fm, "fragment_name");
            }
        });
    }

}
